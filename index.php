<?php require_once "inc/config.php"; ?>
<?php require_once "inc/security.php"; ?>

<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<title>MyTasks - Listing</title>
	<link rel="stylesheet" href="css/app.css">
</head>

<body>
	<?php require "templates/header.php"; ?>

	<div class="row">

		<div class="columns small-12 medium-12 large-12 large-centered">
			<div class="tasklist">


				<ul class="tasklist-title">
					<li>
						<span class="show-for-large tasklist-number-title">#</span>
						<span class="tasklist-description-title">Description<a href="sort/sortID.php"><div class="arrow-down"></div></a></span>
						<span class="show-for-large tasklist-date-title">Crée</span>
						<span class="show-for-large tasklist-author-title">Auteur</span>
						<span class="tasklist-due-title">Due<a href="sort/sortDate.php"><div class="arrow-down"></div></a></span>
						<span class="show-for-large tasklist-assignee-title">Assigné<a href="sort/sortUser.php"><div class="arrow-down"></div></a></span>
						<span class="hide-for-small-only tasklist-priorite-title">Priorité</span>
						<span class="show-for-large tasklist-executer-title">Exécuteur<a href="sort/sortStatus.php"><div class="arrow-down"></div></a></span>
					</li>
				</ul>

				<?php foreach ($tasks as $task) :?>      
					<?php if($task['status'] == "open") : ?>
						<ul class="tasklist-content">	
						<?php else : ?>
							<ul class="tasklist-content done">	
							<?php endif; ?>
							<li>
								<span class="show-for-large tasklist-number"><?php echo $task['id']?></span>
								<span class="tasklist-description"><?php echo $task['description']?></span>
								<span class="show-for-large tasklist-date"><?php echo $task['created_at']?></span>
								<span class="show-for-large tasklist-author"><?php echo $task['author_name']?></span>
								<span class="tasklist-due"><?php echo $task['due_at']?></span>
								<span class="show-for-large tasklist-assignee"><?php echo $task['assignee_name']?></span>
								<span class="hide-for-small-only tasklist-priorite"><?php echo $task['priority']?></span>
								<span class="show-for-large tasklist-executer"><?php echo $task['executer_name']?></span>

								<span class="tasklist-action">
									<a href="edit.php?id=<?php echo $task['id'], $task['status'];?>"><button type="button" class="tasklist-edit">&#9998</button></a>
									<a href="#" data-status="<?php echo $task['id']?>"><button type="button" class="tasklist-check">✔</button></a>
									<a href="#" data-delete="<?php echo $task['id']?>"><button type="button" class="tasklist-cancel">✘</button></a>
								</span>
							</li>
						</ul>
					<?php endforeach ;?>
				</div>
			</div>

		</div>

		<?php require "templates/footer.php"; ?>


		<script src="bower_components/jquery/dist/jquery.js"></script>
		<script src="bower_components/what-input/dist/what-input.js"></script>
		<script src="bower_components/foundation-sites/dist/js/foundation.js"></script>

		<script src="js/app.js"></script>
	</body>
	</html>
