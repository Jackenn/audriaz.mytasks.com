$(document).foundation();

$('[data-delete]').click(function(e) {
	id = $(this).data('delete');
	element = $(this);
	line = element.parent().parent().parent();

	$.ajax({
		url: 'delete.php?task=' + id,
	})

	.done(function(data) {
		if (data == '1') {
			line.fadeOut();
		} else {
			alert("Impossible de supprimer");
		}
	})
	;});


$('[data-status]').click(function(e) {
	id = $(this).data('status');
	element = $(this);
	line = element.parent().parent().parent();

	$.ajax({
		dataType: 'json',
		url: 'done.php?task=' + id,
	})

	.done(function( e ) {
		line.toggleClass('done');
		line.find('.tasklist-executer').html(e.doneBy);

	})
	;});