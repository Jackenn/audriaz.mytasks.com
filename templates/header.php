<header class="row expanded">
	<div class="small-6 medium-6 large-6 columns">
		
		<button class="hamburger" type="button" data-open="offCanvasLeft">&#9776;</button>
		<div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
			<a href="/logout.php"><button type="button" class="header-button">LOGOUT</button></a>
			<a href="filter/all.php"><button type="button" class="header-button-filter">Show all</button></a>
			<a href="filter/active.php"><button type="button" class="header-button-filter">Active</button></a>
			<a href="filter/archived.php"><button type="button" class="header-button-filter">Archived</button></a>
			<button class="close-button" aria-label="Close menu" type="button" data-close>
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		
		<a href="/index.php"><div class="header-title-label">MyTask</div></a>
	</div>
	<div class="small-6 medium-6 large-6 columns">

		<div class="header-title"><img src=<?php echo $user['image']?> alt="profile" class="header-title-img"></div>
	</div>
</div>
</header>
