<?php
require('inc/config.php');

$req = $db->prepare('SELECT due_at FROM task WHERE id=?');
$req->execute(array( $_REQUEST['task'] ));
$data = $req->fetch();

if($data['due_at'] == NULL) {
	$stmt = $db->prepare('UPDATE task SET description=?, assigned_to=?, priority=? WHERE id=?');
	$stmt->execute(array ($_POST['description'], $_POST['assigned'], $_POST['select'], $_POST['id']));

} else {
	$stmt = $db->prepare('UPDATE task SET description=?,due_at=?, assigned_to=?, priority=? WHERE id=?');
	$stmt->execute(array ($_POST['description'], $_POST['due'], $_POST['assigned'], $_POST['select'], $_POST['id']));
}


$tasks = $stmt->fetchAll();

header('location:index.php');

?>
