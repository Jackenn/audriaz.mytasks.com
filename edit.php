<?php
require_once('inc/config.php');
require "inc/security.php";

$id = $_REQUEST['id'];

$stmt = $db->prepare("SELECT * FROM task WHERE id=?");
$stmt->execute(array( $id ));
$task = $stmt->fetch();
?>

<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <title>MyTask - Edit</title>
  <link rel="stylesheet" href="css/app.css">
</head>

<body>
  <?php require "templates/header.php"; ?>

  <div class="row">
    <div class="columns small-12 medium-12 large-12 large-centered">
      <form class="edit-form" method="post" action="update.php">

        <div class="edit">
          <span class="edit-form-label">Description</span>
          <span><textarea name="description" class="edit-form-textarea"><?php if(isset($task)) echo $task['description'];?></textarea></span>
        </div>

        <div class="edit">
          <span class="edit-form-label">Priorité</span>
          <span><select name="select" class="edit-form-select" value="<?php if(isset($task)) echo $task['priority'];?>">

            <option 
            <?php if(isset($task) && $task['priority'] == 1) :?>
              selected="selected"
            <?php endif; ?>
            value="1">1</option>
            <option 
            <?php if(isset($task) && $task['priority'] == 2) :?>
              selected="selected"
            <?php endif; ?>
            value="2">2</option>
            <option 
            <?php if(isset($task) && $task['priority'] == 3) :?>
              selected="selected"
            <?php endif; ?>
            value="3">3</option>
            <option 
            <?php if(isset($task) && $task['priority'] == 4) :?>
              selected="selected"
            <?php endif; ?>
            value="4">4</option>

          </select></span>
        </div>

        <div class="edit">
          <span class="edit-form-label">Due</span>
          <span><input class="edit-form-input" type="date" name="due" value="<?php if(isset($task)) echo $task['due_at'];?>"></span>
        </div>

        <div class="edit">
          <span class="edit-form-label">Assigned to</span>
          <span><select name="assigned" class="edit-form-assigned">
            <?php 
            foreach ($users as $user) :?>
            <option value =<?php echo $user['id']?> 
            <?php if(isset($task)) :?>
              selected="selected"
            <?php endif; ?>
            >
              <?php echo $user['name']?>
            </option>
            <?php
            endforeach ;?>

          </select></span>
        </div>

        <span hidden><textarea name="id" class="edit-form-id"><?php if(isset($task)) echo $task['id'];?></textarea></span>

        <div class="edit-form-send"><input class="edit-form-send-button" type="submit" value="OK" /></div>
      </form>
    </div>
  </div>
  
  <?php require "templates/footerNaked.php"; ?>

  <script src="bower_components/jquery/dist/jquery.js"></script>
  <script src="bower_components/what-input/dist/what-input.js"></script>
  <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
  <script src="js/app.js"></script>
</body>
</html>