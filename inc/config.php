<?php
session_start();

try {
	$db = new PDO('mysql:host=localhost;dbname=task;charset=utf8', 'root', 'root');
} catch(Exception $e) {
	die('Erreur : '.$e->getMessage());
}

$stmt = $db->query('SELECT *, task.id,
	task.description, 
	task.created_at,
	author.name as author_name,
	assignee.name as assignee_name,
	executer.name as executer_name
	FROM task
	INNER JOIN user author ON task.created_by = author.id
	INNER JOIN user assignee ON task.assigned_to = assignee.id
	LEFT JOIN user executer ON task.done_by = executer.id
	ORDER BY task.id
');

$tasks = $stmt->fetchAll(PDO::FETCH_ASSOC);

$st = $db->query('SELECT * FROM user');
$users = $st->fetchAll(PDO::FETCH_ASSOC);
?>