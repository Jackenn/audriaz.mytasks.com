-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`description` text NOT NULL,
	`created_at` date NOT NULL,
	`created_by` varchar(255) DEFAULT NULL,
	`due_at` date DEFAULT NULL,
	`assigned_to` varchar(255) DEFAULT NULL,
	`priority` enum('1','2','3','4') NOT NULL,
	`status` enum('open','closed') NOT NULL,
	`done_by` varchar(255) DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `task` (`id`, `description`, `created_at`, `created_by`, `due_at`, `assigned_to`, `priority`, `status`, `done_by`) VALUES
(39,	'Tâche 1',	'2017-06-25',	'1',	'2017-06-25',	'2',	'1',	'closed',	'2'),
(40,	'Tâche 2',	'2017-06-25',	'1',	NULL,	'1',	'3',	'open',	NULL),
(41,	'Tâche 3\r\n',	'2017-06-25',	'3',	'2017-06-15',	'3',	'4',	'open',	NULL),
(42,	'Tâche 4',	'2017-06-25',	'2',	'2017-06-25',	'1',	'1',	'open',	NULL);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	`email` varchar(255) NOT NULL,
	`password` varchar(255) NOT NULL,
	`image` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `name`, `email`, `password`, `image`) VALUES
(1,	'Patrick Audriaz',	'audriaz',	'root',	'http://i.imgur.com/Pu5OWl4.jpg?1'),
(2,	'Kevin Da Silva',	'silva',	'root',	'http://puu.sh/wqmwq/1ecba588c5.jpg'),
(3,	'Mickael Reynaud',	'Reynaud',	'root',	'http://puu.sh/wqpAn/c689326205.jpg');

-- 2017-06-26 08:28:25