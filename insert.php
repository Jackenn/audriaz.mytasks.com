 <?php
 require "inc/config.php";

 if ($_POST['due'] != NULL){
 	$req = $db->prepare('INSERT INTO `task` (`description`, `created_at`, `created_by`, `due_at`, `assigned_to`, `priority`)
 		VALUES (?, now(), ?, ?, ?, ?)');

 	$req->execute(array ($_POST['description'], $_SESSION['userid'], $_POST['due'] , $_POST['assigned'],  $_POST['select']));
 	
 } else {
 	$req = $db->prepare('INSERT INTO `task` (`description`, `created_at`, `created_by`, `assigned_to`, `priority`)
 		VALUES (?, now(), ?, ?, ?)');

 	$req->execute(array ($_POST['description'], $_SESSION['userid'] , $_POST['assigned'],  $_POST['select']));
 }

 $tasks = $stmt->fetchAll();

 header('location:index.php');
 ?>