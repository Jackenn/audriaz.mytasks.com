<?php require "inc/config.php"; ?>

<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<title>MyTask - Edit</title>
	<link rel="stylesheet" href="css/app.css">
</head>

<body>
	<?php require "templates/headerLogin.php"; ?>

	<div class="row">
		<div class="columns small-12 medium-12 large-12 large-centered">
			<form class="edit-form" method="post" action="insertUser.php">

				<div class="edit">
					<span class="edit-form-label">Nom Prénom</span>
					<span><input name="name" class="new-user-input"></input></span>
				</div>

				<div class="edit">
					<span class="edit-form-label">Email</span>
					<span><input name="email" class="new-email-input"></input></span>
				</div>

				<div class="edit">
					<span class="edit-form-label">Mot de passe</span>
					<span><input name="password" class="new-mdp-input"></input></span>
				</div>

				<div class="edit">
					<span class="edit-form-label">Url image</span>
					<span><input name="image" class="new-image-input"></input></span>
				</div>

				<div class="edit-form-send"><input class="edit-form-send-button" type="submit" value="OK" /></div>
			</form>
		</div>
	</div>

	<?php require "templates/footerNaked.php"; ?>

	<script src="bower_components/jquery/dist/jquery.js"></script>
	<script src="bower_components/what-input/dist/what-input.js"></script>
	<script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
	<script src="js/app.js"></script>
</body>
</html>