<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<title>MyTasks - Login</title>
	<link rel="stylesheet" href="css/app.css">
</head>

<body>
	<?php require "templates/headerLogin.php"; ?>

	<div class="row">
		<div class="columns small-12 medium-12 large-12 large-centered">
			<form class="login-form" method="post" action="logme.php"">

				<div class="login">
					<span class="edit-form-label">Email</span>
					<span><input name="Email" class="login-email-input"></input></span>
				</div>

				<div class="login">
					<span class="edit-form-label">Mot de passe</span>
					<span><input name="Password" class="login-pswd-input"></input></span>
				</div>

				<div class="edit-form-send"><input class="edit-form-send-button" type="submit" value="OK" /></div>
				<div class="edit-form-send"><a href="/new.php"><button type="button" class="edit-form-new-button" />NEW</button></a></div>
			</form>
		</div>
	</div>

	<?php require "templates/footerNaked.php"; ?>
	
	<script src="bower_components/jquery/dist/jquery.js"></script>
	<script src="bower_components/what-input/dist/what-input.js"></script>
	<script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
	<script src="js/app.js"></script>
</body>
</html>
