<?php
require_once "inc/config.php";

$req = $db->prepare('SELECT status FROM task WHERE id=?');
$req->execute( array( $_REQUEST['task'] ) );
$data = $req->fetch();

if($data['status'] == "closed") {
	$sth = $db->prepare("UPDATE task  SET status = 'open', done_by = 0 WHERE id=?");
	$sth-> execute(array ($_REQUEST['task']));
} else {
	$sth = $db->prepare("UPDATE task  SET status = 'closed', done_by =?  WHERE id=?");
	$sth-> execute(array ($_SESSION['userid'], $_REQUEST['task']));
}


$req = $db->prepare("SELECT task.id,done.name doneBy FROM task LEFT JOIN user as done ON done.id=task.done_by WHERE task.id=?");
$req->execute( array( $_REQUEST['task'] ) );
echo json_encode( $req->fetch() );
?>